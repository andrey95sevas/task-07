package com.epam.rd.java.basic.task7.db;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.invoke.ConstantCallSite;
import java.sql.*;
import java.util.*;


import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {
    //jdbc:derby:memory:testdb;create=true

    private static final String DATABASE_URL;

    private static DBManager instance;

    private static final String CREATE_USER = "INSERT INTO users VALUES (DEFAULT, ?)";
    private static final String GET_ALL_USERS = "SELECT * FROM users";

    private static final String CREATE_TEAM = "INSERT INTO teams VALUES (DEFAULT, ?)";
    private static final String GET_ALL_TEAMS = "SELECT * FROM teams";

    private static final String FIND_USER_BY_LOGIN = "SELECT * FROM users WHERE login = ?";
    private static final String FIND_TEAM_BY_NAME = "SELECT * FROM teams WHERE name = ?";

    private static final String INSERT_USER_TEAMS = "INSERT INTO users_teams VALUES (?, ?)";

    private static final String GET_USER_TEAMS = "SELECT team_id FROM users_teams WHERE user_id = ?";

    private static final String DELETE_TEAM_BY_NAME = "DELETE FROM teams WHERE name = ?";

    private static final String SQL_UPDATE_GROUPS_WHERE_NAME = "UPDATE teams SET name = ? WHERE id= ?";

    static {
        Properties properties  = new Properties();
        try{
            properties.load(new FileReader("app.properties"));

        } catch (IOException e) {
            e.printStackTrace();
        }
        DATABASE_URL = (String) properties.get("connection.url");
    }

    private DBManager() {

    }


    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }


    public boolean insertUser(User user) {
        ResultSet rs;
        try (Connection connection = DriverManager.getConnection(DATABASE_URL);
             PreparedStatement preparedStatement = connection.prepareStatement(CREATE_USER, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, user.getLogin());
            if (preparedStatement.executeUpdate() > 0) {
                rs = preparedStatement.getGeneratedKeys();
                if (rs.next()) {
                    user.setId(rs.getInt(1));
                }
            }
        } catch (SQLException exception) {
            System.out.println("something wrong with inserting user");
            exception.printStackTrace();
        }
        return false;
    }

    public List<User> findAllUsers() throws DBException {
        List<User> users = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(DATABASE_URL);
             PreparedStatement preparedStatement = connection.prepareStatement(GET_ALL_USERS)) {
            ResultSet userResultSet = preparedStatement.executeQuery();
            while (userResultSet.next()) {
                users.add(getUserFromResultSet(userResultSet));
            }
        } catch (SQLException exception) {
            System.out.println("something wrong with getting users");
            exception.printStackTrace();
        }
        return users;
    }

    private User getUserFromResultSet(ResultSet userResultSet) throws SQLException {
        User user = new User();
        user.setId(userResultSet.getInt(1));
        user.setLogin(userResultSet.getString(2));
        return user;
    }

    public boolean insertTeam(Team team) {
        ResultSet rs;
        try (Connection connection = DriverManager.getConnection(DATABASE_URL);
             PreparedStatement preparedStatement = connection.prepareStatement(CREATE_TEAM, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, team.getName());
            if (preparedStatement.executeUpdate() > 0) {
                rs = preparedStatement.getGeneratedKeys();
                if (rs.next()) {
                    team.setId(rs.getInt(1));
                }
                return true;
            }
        } catch (SQLException exception) {
            System.out.println("something wrong with inserting team");
            exception.printStackTrace();
        }
        return false;
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> teams = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(DATABASE_URL);
             PreparedStatement preparedStatement = connection.prepareStatement(GET_ALL_TEAMS)) {
            ResultSet teamResultSet = preparedStatement.executeQuery();
            while (teamResultSet.next()) {
                teams.add(getTeamFromResultSet(teamResultSet));
            }
        } catch (SQLException exception) {
            System.out.println("something wrong with getting users");
            exception.printStackTrace();
        }
        return teams;
    }

    private Team getTeamFromResultSet(ResultSet userResultSet) throws SQLException {
        Team team = new Team();
        team.setId(userResultSet.getInt(1));
        team.setName(userResultSet.getString(2));
        return team;
    }

    public User getUser(String login) throws DBException {
        User user = new User();
        try (Connection connection = DriverManager.getConnection(DATABASE_URL);
             PreparedStatement preparedStatement = connection.prepareStatement(FIND_USER_BY_LOGIN)) {
            preparedStatement.setString(1, login);
            ResultSet userResultSet = preparedStatement.executeQuery();
            while (userResultSet.next()) {
                user = getUserFromResultSet(userResultSet);

            }
        } catch (SQLException e) {
            System.out.println("something wrong with getting USER by LOGIN");
            e.printStackTrace();
        }
        return user;
    }

    public Team getTeam(String name) throws DBException {
        Team team = new Team();
        try (Connection connection = DriverManager.getConnection(DATABASE_URL);
             PreparedStatement preparedStatement = connection.prepareStatement(FIND_TEAM_BY_NAME)) {
            preparedStatement.setString(1, name);
            ResultSet teamResultSet = preparedStatement.executeQuery();
            while (teamResultSet.next()) {
                team = getTeamFromResultSet(teamResultSet);
            }
        } catch (SQLException e) {
            System.out.println("something wrong with getting TEAM by NAME");
            e.printStackTrace();
        }
        return team;
    }

    public boolean setTeamForUser(Connection con, User user, Team team) throws DBException, SQLException {
        try
            (PreparedStatement preparedStatement1 = con.prepareStatement(INSERT_USER_TEAMS)){
            con.setAutoCommit(false);
            preparedStatement1.setInt(1, user.getId());
            preparedStatement1.setInt(2, team.getId());
            if (preparedStatement1.executeUpdate() != 1) {
                return false;
            }
            return true;
        } catch (SQLException exception) {
            throw new DBException("oops, transaction problem", exception);
        }
    }


    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        if (user == null) {
            return false;
        }
        for (Team t : teams){
            if (t == null) {
                return false;
            }
        }
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(DATABASE_URL);
            connection.setAutoCommit(false);
            for (Team team : teams) {
                setTeamForUser(connection, user, team);
            }
            connection.commit();
            return true;
        } catch (DBException | SQLException e) {
            try {
                Objects.requireNonNull(connection).rollback();
                throw new DBException();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return false;
    }

    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> teams = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(DATABASE_URL);
             PreparedStatement preparedStatement = connection.prepareStatement(GET_USER_TEAMS)) {
            preparedStatement.setInt(1, user.getId());
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                teams.add(getTeamById(rs.getInt(1)));
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("sad", e);
        }
        return teams;
    }

    private Team getTeamById(int id) {
        Team team = null;
        try (Connection connection =DriverManager.getConnection(DATABASE_URL);
             PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM teams where id = ?")) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                team = getTeamFromResultSet(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return team;
    }

    public boolean deleteTeam(Team team) throws DBException {
        try (Connection connection = DriverManager.getConnection(DATABASE_URL);
             PreparedStatement preparedStatement = connection.prepareStatement(DELETE_TEAM_BY_NAME)) {
            preparedStatement.setString(1, team.getName());
            if (preparedStatement.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException e) {
            System.out.println("something wrong with deleting team by name");
            e.printStackTrace();
        }
        return false;
    }

    public void updateTeam(Team teamA) throws DBException {
        try (Connection con = DriverManager.getConnection(DATABASE_URL);
             PreparedStatement preparedStatement = con.prepareStatement(SQL_UPDATE_GROUPS_WHERE_NAME)) {
            preparedStatement.setString(1, teamA.getName());
            preparedStatement.setInt(2, teamA.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            //e.printStackTrace();
            throw new DBException("BAD", e);
        }
    }
//
//    public boolean deleteUsers(User... users) throws DBException{
//        if (users.length == 0) {
//            return false;
//        }
//        StringJoiner joiner = new StringJoiner(",");
//        for (User user : users){
//            joiner.add(String.valueOf(user.getId()));
//        }
//        String query = String.format("delete from users where id in(%s)", joiner.toString());
//        try (Connection connection = DriverManager.getConnection(DATABASE_URL);
//             PreparedStatement preparedStatement = connection.prepareStatement(query)){
//            preparedStatement.executeUpdate();
//        } catch (SQLException e) {
//            throw new DBException();
//        }
//        return true;
//    }
}
